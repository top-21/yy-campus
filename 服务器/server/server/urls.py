
from django.contrib import admin
from django.urls import path
from web.utils import views, select_sql_api, upfile
from django.conf.urls.static import static
from django.conf import settings
urlpatterns = [
    path('index/', views.view_index),

    # 商店
    path('store/create/', views.view_create_store),
    path('store/goods/create/nav/', views.view_create_nav),
    path('store/create/goods/', views.view_create_products),

    # api
    path('api/select/store/', select_sql_api.api_select_store_id),
    path('api/select/store/all/', select_sql_api.api_select_store_all),
    path('api/select/store/goods/', select_sql_api.api_select_store_nav_goods_all),


    # 上传api
    path('api/upfile/store/images/',upfile.store_images ),
]
urlpatterns += static(settings.STATIC_URL, document_root=settings.MEDIA_ROOT)
