from django.db import models

# Create your models here.

"""
1. 生成迁移文件

python manage.py makemigrations

2. 同步到数据库中

python manage.py migrate
"""


# 商品表
class products(models.Model):
    name = models.CharField(verbose_name='商品名称', max_length=50)
    description = models.CharField(verbose_name='商品详细描述', max_length=200, null=True, blank=True)
    price = models.CharField(verbose_name='商品价格', max_length=20)
    stock = models.BigIntegerField(verbose_name='商品库存数量', default=100, null=True, blank=True)
    sku = models.CharField(verbose_name='商品唯一标识码', max_length=200)
    # image_url = models.CharField(verbose_name='商品主图URL', max_length=200, default='yy_demo1.jpg', null=True, blank=True)
    # images = models.TextField(verbose_name='存储多个商品图片的URL，可以用逗号分隔方式存储', default='yy_demo1.jpg', null=True, blank=True)
    # 1活动 2不活动 3已删除
    status = models.BigIntegerField(verbose_name='商品状态（活动、不活动、已删除）', default='1', null=True, blank=True)
    created_at = models.DateTimeField(verbose_name='商品创建时间', auto_now_add=True)
    updated_at = models.DateTimeField(verbose_name='商品最后更新时间', auto_now=True)
    brand_id = models.ForeignKey(verbose_name='关联的商家', to='vendors', on_delete=models.CASCADE)
    VendorGoodsNavigator_id = models.ForeignKey(verbose_name='关联的导航', to='VendorGoodsNavigator',
                                                on_delete=models.CASCADE)


# 店家表 vendors
class vendors(models.Model):
    name = models.CharField(unique=True, verbose_name='商店名称', max_length=30, error_messages={
        "required": "商店名称不能为空",
    })
    vendorsPhone = models.CharField(verbose_name='商家电话', max_length=200)
    score = models.FloatField(verbose_name='评分', default=0, null=True, blank=True)
    address = models.CharField(verbose_name='商家地址', max_length=200, default='互联网', null=True, blank=True)
    announcements = models.TextField(verbose_name='商家公告', default='该商家尚未发布公告噢!', null=True, blank=True)

    def __str__(self):
        return self.name



# 商家-商品导航 VendorGoodsNavigator
class VendorGoodsNavigator(models.Model):
    vendors_id = models.ForeignKey(verbose_name='关联商家', to='vendors', on_delete=models.CASCADE)
    name = models.CharField(verbose_name='导航名称', max_length=20)
    update_to = models.DateTimeField(verbose_name='最后一次修改时间', auto_now=True)

    def __str__(self):
        return self.name

# 封面图和商品图
class images(models.Model):
    title = models.CharField(verbose_name='图片名称', max_length=50)
    image_url = models.FileField(verbose_name='图片路径', upload_to='store/images', default='', null=True, blank=True)
    # 种类 1-商家封面图    2-商品封面图
    image_type = models.BigIntegerField(verbose_name='图片种类')
    # 关联的id
    image_id = models.CharField(verbose_name='关联的id', max_length=20)
