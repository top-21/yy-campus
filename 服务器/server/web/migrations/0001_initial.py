# Generated by Django 5.0.4 on 2024-05-08 05:27

import django.db.models.deletion
from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='vendors',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=30, verbose_name='商店名称')),
                ('announcement', models.TextField(verbose_name='商家公告')),
                ('vendorsPhone', models.CharField(max_length=200, verbose_name='商家电话')),
                ('address', models.CharField(max_length=200, verbose_name='商家地址')),
            ],
        ),
        migrations.CreateModel(
            name='VendorGoodsNavigator',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=20, verbose_name='导航名称')),
                ('update_to', models.DateTimeField(auto_now=True, verbose_name='最后一次修改时间')),
                ('vendors_id', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='web.vendors', verbose_name='关联商家')),
            ],
        ),
        migrations.CreateModel(
            name='products',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=50, verbose_name='商品名称')),
                ('description', models.CharField(max_length=200, verbose_name='商品详细描述')),
                ('price', models.CharField(max_length=20, verbose_name='商品价格')),
                ('stock', models.BigIntegerField(verbose_name='商品库存数量')),
                ('sku', models.CharField(max_length=200, verbose_name='商品唯一标识码')),
                ('image_url', models.CharField(max_length=200, verbose_name='商品主图URL')),
                ('images', models.TextField(verbose_name='存储多个商品图片的URL，可以用逗号分隔方式存储')),
                ('status', models.BigIntegerField(verbose_name='商品状态（活动、不活动、已删除）')),
                ('created_at', models.BigIntegerField(verbose_name='商品创建时间')),
                ('updated_at', models.BigIntegerField(verbose_name='商品最后更新时间')),
                ('VendorGoodsNavigator_id', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='web.vendorgoodsnavigator', verbose_name='关联的导航')),
                ('brand_id', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='web.vendors', verbose_name='关联的商家')),
            ],
        ),
    ]
