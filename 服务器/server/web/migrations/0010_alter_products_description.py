# Generated by Django 5.0.4 on 2024-05-08 13:50

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('web', '0009_alter_products_status'),
    ]

    operations = [
        migrations.AlterField(
            model_name='products',
            name='description',
            field=models.CharField(blank=True, max_length=200, null=True, verbose_name='商品详细描述'),
        ),
    ]
