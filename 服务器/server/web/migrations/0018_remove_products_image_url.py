# Generated by Django 5.0.4 on 2024-05-09 04:29

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('web', '0017_remove_products_images'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='products',
            name='image_url',
        ),
    ]
