// 请确保在使用时引入了 axios 

const DOMAIN = "http://127.0.0.1:10000/"
async function YYAXIOS(Obj) {
    let response = null;
    if (Obj.method == "GET") {
        response = await axios({
            url: DOMAIN + Obj.url,
            method: Obj.method,
            headres: {
                "Content-type": "application/x-www-form-urlencoded",
            },
            params: Obj.params
        });
    } else {
        response = await axios({
            url: DOMAIN + Obj.url,
            method: Obj.method,
            headres: {
                "Content-type": "application/x-www-form-urlencoded",
            },
            data: JSON.stringify(Obj.params)
        });
    }
    return response;
    
}


// 用语
const yy_yuyan = {
    err: {
        "e1": "新增失败",
        "e2": "添加失败"
    },
    success: {
        "s1": "新增成功",
        "s2": "添加成功"
    }
}



// layui
const yy_layui = {
    // 信息框
    layui_alert: function(title, centent, icon) {
        let index = layer.alert(centent, {
            title: title,
            icon: icon,
        }, () => {
            layer.close(index);
        })
    },
    // msg
    layui_msg: function(test) {
        layer.msg(test);
    }
}

