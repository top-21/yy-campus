
// Loading框
function YYLoadings(title, Time) {
	uni.showLoading({
		title: title,
		mask: false
	});
	setTimeout(() => {
		uni.hideLoading()
	}, Time)
}


module.exports = {
	"YYLoadings": YYLoadings
}
