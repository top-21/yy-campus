表
id-标识
title-店铺名称
score-店铺评分
startPrice-起送价
endPrice-配送费
honor-店铺荣誉	（由官方提供，排名可考前）
monthlySales-月销量
bulletin-公告
commit-该店铺的评论
bgc-商店背景图
image-商家封面图

表
commodityTitle-商品导航（关联商品）

表
商品区
goods-商品
goodsPrice-商品价格
goodsSales-商品销量
goodsInfo-商品详情
goodsId-商品id（商品的唯一标识）
commit-该商品的评论
image-商品封面图



